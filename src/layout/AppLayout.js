import React from "react";
import { useRouter } from "next/dist/client/router";
import { ModalProvider } from "../hook/modal";
import { AlertProvider } from "../hook/alert";
import Header from "../component/Header";
import Footer from "../component/Footer";
import Banner from "../component/Banner";
import Navbar from "../component/Navbar";
import TaskBar from "../component/TaskBar";
import { useAuth } from "../hook/auth";


export default function AppLayout({ children }) {
	const { user } = useAuth();
	const router = useRouter();
	let prefix = router.pathname.split('/');

	return (
		<>
			<Header pageName={(prefix[1] ? prefix[1] + ' | ' : '') + 'Attendance Management'} />
			<ModalProvider>
				<AlertProvider>
					<div className="container">
						<Banner />
						<Navbar />
						<br />
						{
							prefix[1] === 'member' && user && user.role &&
							(user.role.member || user.role.admin) && <TaskBar />
						}
						<br />
						<div className="container-padding">
							{children}
						</div>
						<br /><br /><br /><br /><br />
						<Footer />
					</div>
				</AlertProvider>
			</ModalProvider>
		</>
	)

}

// (
// 	<div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
// 		<h3>app loading...</h3>
// 	</div>
// )
