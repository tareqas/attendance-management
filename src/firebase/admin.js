import * as admin from "firebase-admin";
import serviceAccount from "./admin.config.json";


export default function adminInit() {
	if (!admin.apps.length) {
		admin.initializeApp({
			credential: admin.credential.cert(serviceAccount),
			storageBucket: "attendance-management-b9346.appspot.com",
			databaseURL: "https://attendance-management-b9346-default-rtdb.firebaseio.com",
		});
	}
}

export function verifyIdToken(token) {
	adminInit();
	return admin.auth()
		.verifyIdToken(token)
		.catch((err) => {
			console.log('Error verifying user token:', err);
			return err;
		});
};

export function getUser(uid) {
	adminInit();
	return admin.auth()
		.getUser(uid)
		.catch((err) => {
			console.log('Error fetching user data:', err);
			return err;
		});
}

export function getUserByEmail(email) {
	adminInit();
	return admin.auth()
		.getUserByEmail(email)
		.catch((err) => {
			console.log('Error fetching user data:', err);
			return err;
		});
}

/**
 * boject sample
 * {
	email: 'user@example.com',
	emailVerified: false,
	phoneNumber: '+11234567890',
	password: 'secretPassword',
	displayName: 'John Doe',
	photoURL: 'http://www.example.com/12345678/photo.png',
	disabled: false,
  }
 */
export function createUser(obj) {
	adminInit();
	return admin.auth()
		.createUser(obj)
		.catch((err) => {
			console.log('Error creating new user:', err);
			return err;
		});
}

export function updateUser(uid, obj) {
	adminInit();
	return admin.auth()
		.updateUser(uid, obj)
		.catch((err) => {
			console.log('Error creating new user:', err);
			return err;
		});
}

export function deleteUser(uid) {
	adminInit();
	return admin.auth()
		.deleteUser(uid)
		.then(() => true)
		.catch((err) => {
			console.log('Error deleting user:', err);
			return err;
		});
}

export function addRole(uid, obj) {
	adminInit();
	return admin.database()
		.ref().set({
			users: {
				'tareqahamedbd@gmail.com': {
					role: admin,
					data: []
				}
			}
		})
		.catch((err) => {
			console.log('Error setting custom user claims:', err);
			return err;
		});
}

// export function addOrChangeRole(uid, obj) {
//   return new Promise(async (resolve, reject) => {
//     let user = await setCustomUserClaims(uid, obj).catch((err) => {
//       console.log('Error getting public url:', err);
//       return err
//     });
//     // if (user.code) resolve(user);
//     // else {
//     //   // let user = await admin.firestore().collection('users').doc('tareqahamedbd@gmail.com');
//     //   console.log('user');
//     // }
//   })

// }

// { admin: true } / { member: true }
export function setCustomUserClaims(uid, obj) {
	adminInit();
	return admin.auth()
		.setCustomUserClaims(uid, obj)
		.then(() => true)
		.catch((err) => {
			console.log('Error setting custom user claims:', err);
			return err;
		});
}

export function listAllUsers(nextPageToken) {
	// List batch of users, 1000 at a time.
	adminInit();
	return new Promise((resolve, reject) => {
		admin.auth()
			.listUsers(100, nextPageToken)
			.then((listUsersResult) => {
				let info = {
					users: []
				};
				listUsersResult.users.forEach((userRecord) => {
					info.users.push(userRecord.toJSON());
				});
				info.pageToken = (listUsersResult.pageToken) ? listUsersResult.pageToken : null;
				resolve(info);
			})
			.catch((err) => {
				console.log('Error listing users:', err);
				resolve(err);
			});
	})
};

export function getPublicUrl(uploadedFile) {
	adminInit();
	return new Promise(async (resolve, reject) => {
		const bucket = admin.storage().bucket('gs://attendance-management-b9346.appspot.com');
		const file = await bucket.file(uploadedFile).makePublic().catch((err) => {
			console.log('Error getting public url:', err);
			return err
		});
		// // const [metadata] = file.getMetadata();
		// // const url = metadata.mediaLink;
		if (file[0] && file[0].bucket) {
			let url = `https://storage.googleapis.com/${file[0].bucket}/${file[0].object}`;
			resolve(url);
		} else resolve(file);
	})
}