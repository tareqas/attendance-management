import * as firebase from "firebase/app";
import { getDatabase, ref as refDatabase, set, onValue } from "firebase/database";
import { getStorage, ref as refStorage, uploadBytes } from "firebase/storage";
import clientConfig from "./client.config";


export default function clientInit() {
	if (!firebase.getApps().length) {
		firebase.initializeApp(clientConfig);
	}
}

export function addOrUpdateRole(uid, obj) {
	clientInit();
	return set(
		refDatabase(getDatabase(), `users/${uid}/role`), obj)
		.catch((err) => {
		console.log('Error modifying user role:', err);
		return err
	});
}

export function getUserRole(uid) {
	clientInit();
	return new Promise((resolve, reject) => {
		onValue(refDatabase(getDatabase(), `users/${uid}/role`), (snapshot) => {
			resolve(snapshot.val());
		});
	})
}

export function addEntry(uid, obj) {
	clientInit();
	let collection = obj.workingDate.slice(0, -3) // 2021-12-19 -> 2021-12
	return set(
		refDatabase(getDatabase(), `users/${uid}/workhour/${collection}/${obj.workingDate}`), obj
	).catch((err) => {
		console.log('Error adding entry:', err);
		return err
	});
}

export function deleteEntry(uid, workingDate) {
	clientInit();
	let collection = workingDate.slice(0, -3) // 2021-12-19 -> 2021-12
	return set(
		refDatabase(getDatabase(), `users/${uid}/workhour/${collection}/${workingDate}`), null
	).catch((err) => {
		console.log('Error deleting entry:', err);
		return err
	});
}

export function getEntry(uid, workingDate) {
	clientInit();
	let collection = workingDate.slice(0, -3) // 2021-12-19 -> 2021-12
	return new Promise((resolve, reject) => {
		onValue(refDatabase(getDatabase(),`users/${uid}/workhour/${collection}/${workingDate}`), (snapshot) => {
			resolve(snapshot.val());
		});
	})
}

export function getAttendance(uid, workingDate) {
	clientInit();
	let collection = workingDate.slice(0, -3) // 2021-12-19 -> 2021-12
	return new Promise((resolve, reject) => {
		onValue(refDatabase(getDatabase(),`users/${uid}/workhour/${collection}`), (snapshot) => {
			resolve(snapshot.val());
		});
	})
}

export function uploadFile(profileImgUrl, selectedFile) {
	clientInit();
	return uploadBytes(
		refStorage(getStorage(), profileImgUrl), selectedFile
	).catch((err) => {
		console.log('Error uploading file:', err);
		return err
	});
}


