import { verifyIdToken } from "../firebase/admin";


export default function authMiddleware(req, res) {
    return new Promise(async (resolve, reject) => {
        try {
            const userInfo = await verifyIdToken(req.cookies.token);
            req.locals = userInfo;
            if (userInfo && userInfo.uid && userInfo.email) resolve(true);
            else res.status(403).json({ code: 403, message: '403 Forbidden' });
        } catch (err) {
            res.status(403).json({ code: err.code, message: err.message });
        }
    })
};