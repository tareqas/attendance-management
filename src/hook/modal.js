import React, { useState, useContext, createContext } from "react";
import Modal from "../component/Modal";
const ModalContext = createContext({});


export const useModal = () => useContext(ModalContext);

export const ModalProvider = ({ children }) => {
    const [modal, setModal] = useState({ show: false });

    return (
        <ModalContext.Provider value={{ modal, setModal }}>
            <Modal closeHandler={() => setModal({show: false})} {...modal}/>
            {children}
        </ModalContext.Provider>
    )
}
