import React, { useState, useContext, createContext } from "react";
import Alert from "../component/Alert";
const AlertContext = createContext({});


export const useAlert = () => useContext(AlertContext);

export const AlertProvider = ({ children }) => {
    const [alert, setAlert] = useState({ show: false, message: 'Hey, there!' });

    return (
        <AlertContext.Provider value={{ alert, setAlert }}>
            <Alert closeHandler={() => setAlert({show: false})} {...alert}/>
            {children}
        </AlertContext.Provider>
    )
}
