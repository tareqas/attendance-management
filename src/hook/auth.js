import React, { useState, useEffect, useContext, createContext } from "react";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { parseCookies, setCookie, destroyCookie } from 'nookies'
import firebaseClientInit from "../firebase/client";
import * as firebaseClient from "../firebase/client";
const AuthContext = createContext({});


export const useAuth = () => useContext(AuthContext);

export const AuthProvider = ({ children }) => {
    firebaseClientInit();
    const [user, setUser] = useState(null);

    useEffect(() => {
        const auth = getAuth();
        onAuthStateChanged(auth, async (user) => {
            if (user) {
                let info = await firebaseClient.getUserRole(user.uid);
                user.role = info;
                console.log('----------------> AuthProvider:', user);
                setUser(user);
                const token = await user.getIdToken(true);
                setCookie(undefined, "token", token, {});
            } else {
                setUser({ role: { general: true } });
                console.log('----------------> AuthProvider else:', user);

                destroyCookie(undefined, "token");
                return;
            }
        })
    }, [])

    return (
        <AuthContext.Provider value={{ user }}>{children}</AuthContext.Provider>
    )

}
