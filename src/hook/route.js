import React, { useEffect } from "react";
import { useRouter } from "next/router"
import { useAuth } from "./auth";
import Loading from "../component/Loading";


export function WithProtected(Component) {
	return (props) => {
		const { user } = useAuth();
		const router = useRouter();
		let parts = router.pathname.split('/');
		let userRole = parts[1] ? parts[1] : '';

		useEffect(() => {
			if (user && user.role && user.role.general) {
				router.push('/login');
			}
		}, [user])

		return (
			<>
				{user && user.role && (user.role.admin || (user.role.member && userRole === 'member'))
				? <Component {...props} /> : <Loading />}
			</>
		)
	}

}
