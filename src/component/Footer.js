import Link from "next/link";
import { Fragment } from "react";

export default function Footer() {
    return (
        <Fragment>
            <footer className="blog-footer text-center">
                <Link href="/">Attendance Management</Link> by <a href="https://gitlab.com/tareqas">tareqas</a>
            </footer>
            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
            <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/popper.min.js"></script>
            <script src="https://getbootstrap.com/docs/4.0/dist/js/bootstrap.min.js"></script>
        </Fragment>
    )
}
