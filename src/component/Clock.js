import moment from "moment";
import { useState, useEffect } from "react";


export default function Clock() {
    const [time, setTime] = useState(moment().format('LTS'));

    useEffect(() => {
        let _id = setInterval(() => {
            setTime(moment().format('LTS'));
        }, 1000)
        return () => {
            clearInterval(_id);
        }
    }, [])

    return <>{time}</>

}
