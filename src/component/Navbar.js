import Link from "next/link";
import { getAuth, signInWithEmailAndPassword, createUserWithEmailAndPassword, signOut } from "firebase/auth";
import { useAuth } from "../hook/auth";
import { useModal } from "../hook/modal";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";


export default function Navbar(props) {
    const router = useRouter(); useRouter
    const { user } = useAuth();
    const { modal, setModal } = useModal();
    const [isLogout, setIslogout] = useState(false);

    useEffect(() => {
        if (isLogout) router.push('/');
    }, [isLogout, user])

    function localStorageHandler(e) {
        if (e) e.preventDefault();
        window.localStorage.clear();
        setModal({ show: true, title: "Local Storage", body: 'Local Storage has been wiped out!' });
    }

    function logoutHandler(e) {
        e.preventDefault();
        const auth = getAuth();
        signOut(auth).then(() => {
            setIslogout(true);
            window.localStorage.clear();
        })
            .catch((error) => {
                console.log(error);
                setModal({ show: true, title: "Logout failed!", body: `[${error.code}] ${error.message}` });
            });
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-custom">
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
                <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li className="nav-item">
                        <Link className="nav-link" href="/">Home</Link>
                    </li>
                    {
                        user && user.role && user.role.admin ? (
                            <>
                                <li className="nav-item">
                                    <Link className="nav-link" href="/admin">Admin</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" href="/member">Member</Link>
                                </li>
                            </>
                        ) :
                            user && user.role && user.role.member ? (
                                <li className="nav-item">
                                    <Link className="nav-link" href="/member">Member</Link>
                                </li>
                            ) : (<></>)
                    }
                    <li className="nav-item">
                        <Link className="nav-link" href="/login">
                            {(user && user.role && (user.role.admin || user.role.member)) ? 'Log out' : 'Login'}
                        </Link>
                    </li>
                </ul>
                <ul className="nav-item dropdown my-2 my-lg-0">
                    <span>{user && user.displayName && 'Hello, ' + user.displayName}</span>
                </ul>
                <ul className="nav-item dropdown my-2 my-lg-0">
                    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        More
                    </a>
                    {
                        user && user.role && user.role.admin ? (
                            <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <h6 className="dropdown-header">Admin</h6>
                                &nbsp;&nbsp;<Link className="dropdown-item" href="/admin/new">Add Employee</Link><br />
                                &nbsp;&nbsp;<Link className="dropdown-item" href="/admin">View Employee</Link><br />
                                <h6 className="dropdown-header">Member</h6>
                                &nbsp;&nbsp;<Link className="dropdown-item" href="/member/add">Add Attendance</Link><br />
                                &nbsp;&nbsp;<Link className="dropdown-item" href="/member">Update Attendance</Link><br />
                                <h6 className="dropdown-header">General</h6>
                                &nbsp;&nbsp;<Link className="dropdown-item" href="#">
                                    <a href='#' onClick={localStorageHandler}>Clear LocalStorage</a>
                                </Link><br />
                                &nbsp;&nbsp;<Link className="dropdown-item" href="#">
                                    <a href='#' onClick={logoutHandler}>Log out</a>
                                </Link><br />
                            </div>
                        ) :
                            user && user.role && user.role.member ? (
                                <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <h6 className="dropdown-header">Member</h6>
                                    &nbsp;&nbsp;<Link className="dropdown-item" href="/member/add">Add Attendance</Link><br />
                                    &nbsp;&nbsp;<Link className="dropdown-item" href="/member">Update Attendance</Link><br />
                                    <h6 className="dropdown-header">General</h6>
                                    &nbsp;&nbsp;<Link className="dropdown-item" href="#">
                                        <a href='#' onClick={localStorageHandler}>Clear LocalStorage</a>
                                    </Link><br />
                                    &nbsp;&nbsp;<Link className="dropdown-item" href="#">
                                        <a href='#' onClick={logoutHandler}>Log out</a>
                                    </Link><br />
                                </div>
                            ) :
                                (
                                    <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <h6 className="dropdown-header">General</h6>
                                        &nbsp;&nbsp;<Link className="dropdown-item" href="#">
                                            <a href='#' onClick={localStorageHandler}>Clear LocalStorage</a>
                                        </Link><br />
                                        &nbsp;&nbsp;<Link className="dropdown-item" href="/login">Log in</Link><br />
                                    </div>
                                )
                    }

                </ul>
            </div>
        </nav>
    )

}
