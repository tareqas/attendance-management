import { useEffect, useState } from "react"
import * as firebaseClient from "../../src/firebase/client";
import { useModal } from "../hook/modal";
import { useAlert } from "../hook/alert";
import UserInfoForm from "./UserInfoForm";
import Loading from "./Loading";
import { useRouter } from "next/router";


export default function Create({ isUpdate, user }) {
    const [btnText, setBtnText] = useState(isUpdate ? 'Update' : 'Create');
    const [formInfo, setFormInfo] = useState({});
    const { modal, setModal } = useModal();
    const { alert, setAlert } = useAlert();
    const router = useRouter();

    useEffect(() => {
        setBtnText(isUpdate ? 'Update' : 'Create');
        if (formInfo.email) {
            (async () => {
                console.log(formInfo);
                setBtnText(isUpdate ? 'Updating...' : 'Creating...');
                // images/profile/1639655616722.jpeg
                const profileImgUrl = formInfo.selectedFile ?
                    `images/profile/${new Date().getTime()}.${formInfo.selectedFile.name.split('.').reverse()[0]}` : '';
                let infos = {
                    displayName: formInfo.displayName,
                    email: formInfo.email,
                    password: formInfo.password,
                    role: formInfo.role,
                    photoURL: profileImgUrl
                }
                if (!infos.photoURL) delete infos.photoURL;
                const reqUrl = isUpdate ? '/api/admin/update/' + user.uid : '/api/admin/new';
                let imgInfo = infos.photoURL ? await firebaseClient.uploadFile(profileImgUrl, formInfo.selectedFile) : null;

                if (imgInfo && imgInfo.code) {
                    let _title = isUpdate ? 'Update failed!' : 'Creation failed!';
                    setModal({ show: true, title: _title, body: `[${err.code}] ${err.message}` });
                } else {
                    fetch(reqUrl, {
                        method: 'POST',
                        body: JSON.stringify(infos)
                    }).then(async (res) => {
                        let _res = await res.json();
                        console.log(_res);
                        // create/update role of user
                        if (_res.status) await firebaseClient.addOrUpdateRole(_res.extra.uid, { [infos.role]: true });
                        let title = '';
                        if (isUpdate) title = _res.status ? "Updated successfully!" : "Update failed!";
                        else title = _res.status ? "Added successfully!" : "Creation failed!";
                        if (_res.status) {
                            setAlert({ show: true,
                                message: `Employee named <${infos.displayName}> has been ${isUpdate ? 'updated' : 'created'} with email <${infos.email}>`,
                                type: 'success'
                            });
                            router.push('/admin');
                        } else {
                            setBtnText('Try again!');
                            setModal({ show: true, title: title, body: `[${_res.code}] ${_res.message}` });
                        }
                    }).catch((err) => {
                        console.log(err);
                        setBtnText('Try again!');
                        let _title = isUpdate ? 'Update failed!' : 'Creation failed!';
                        setModal({ show: true, title: _title, body: `[${err.code}] ${err.message}` });
                    });
                }

            })()
        }
    }, [formInfo]);

    if (!isUpdate || (isUpdate && user && user.uid)) {
        return (
            <UserInfoForm
                user={user || {}}
                setFormInfo={setFormInfo}
                setBtnText={setBtnText}
                btnText={btnText}
            />
        )
    } else return <Loading />

}