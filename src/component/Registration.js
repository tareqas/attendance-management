import Link from "next/link";

export default function Registration(props) {
    return (
        <form className="form-signin">
            <h1 className="h3 mb-3 font-weight-normal">Please Registration</h1>
            <label htmlFor="fullname" className="sr-only">Full name</label>
            <input type="text" id="fullname" className="form-control" placeholder="Full name" required autoFocus />
            <label htmlFor="email" className="sr-only">Email address</label>
            <input type="email" id="email" className="form-control" placeholder="Email address" required autoFocus />
            <label htmlFor="pass" className="sr-only">Password</label>
            <input type="password" id="pass" className="form-control" placeholder="Password" required />
            <button className="btn btn-lg btn-primary btn-block" type="submit">Register</button>
            <div className="text-center">
                <h5>Have Account! <Link href="/login">Login Here</Link></h5>
            </div>
        </form>
    );
}
