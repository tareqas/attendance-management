export default function TaskMsg({ closeHandler, show, message, type }) {
    const showHide = show ? { display: "block" } : { display: "none" };
    const classes = `alert alert-${type ? type : 'info'} alert-dismissible fade show text-center`

    return (
        <div className={classes} role="alert" style={showHide}>
            <span>{message}</span>
            <button type="button" className="close" onClick={closeHandler}>
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    )

}