import Head from "next/head"

export default function Header(props) {
  return (
    <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>{ props.pageName }</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
        <link href="/styles/custom.css" rel="stylesheet" />
    </Head>
  )
}
