import moment from "moment";
import { useEffect, useRef, useState } from "react";
import { useAuth } from "../hook/auth";
import { useModal } from "../hook/modal";
import { useAlert } from "../hook/alert";
import * as firebaseClient from "../firebase/client";
import Clock from "./Clock"


export default function TaskBar(props) {

    const { user } = useAuth();
    const [activeBtn, setActiveBtn] = useState();
    const [showMemo, setShowMemo] = useState({ display: "none" });
    const [submitBtn, setSubmitBtn] = useState('Submit');
    const textareaRef = useRef();
    const { modal, setModal } = useModal();
    const { alert, setAlert } = useAlert();

    useEffect(() => {
        let { workingDate, data } = getLocalEntry();
        if (data && data[workingDate] && data[workingDate].finishWorking) setShowMemo({ display: "block" });

        if (!activeBtn && data && data.activeBtn) {
            setActiveBtn(data.activeBtn);
        }
        else if (document.querySelector(`a[data-task="${activeBtn}"]`)) {
            data.activeBtn = activeBtn;
            document.querySelector(`a[data-task="${activeBtn}"]`).style.backgroundColor = '#ededed';
            window.localStorage.setItem(workingDate, JSON.stringify(data));
            let message = `You're working from: `
            message += (data[workingDate].startWorking) ? data[workingDate].startWorking : '';
            message += (data[workingDate].finishWorking) ? ' to: ' + data[workingDate].finishWorking : '';
            setAlert({ show: true, message, type: 'info' });
        }
    }, [activeBtn])

    function getLocalEntry() {
        let workingDate = moment().format('YYYY-MM-DD'); // '2021_12_18' used as a key of working data
        let data = window.localStorage.getItem(workingDate);
        data = data ? JSON.parse(data) : data;
        return { workingDate: workingDate, data: data };
    }

    function clearActiveBtn() {
        document.querySelectorAll('.taskbar a').forEach(elem => {
            elem.style.backgroundColor = 'white';
        });
    }

    function isWorkRunning() {
        let { workingDate, data } = getLocalEntry();
        return (data && data[workingDate] && data[workingDate].startWorking && !data[workingDate].finishWorking)
            ? true : false;
    }

    function isBreakRunning() {
        let { workingDate, data } = getLocalEntry();
        return (data && data[workingDate] && data[workingDate].breaks && data[workingDate].breaks.length > 0 &&
            !data[workingDate].breaks[data[workingDate].breaks.length - 1].finishBreak)
            ? true : false;
    }

    function isWorkDone() {
        let { workingDate, data } = getLocalEntry();
        return (data && data[workingDate] && data[workingDate].startWorking && data[workingDate].finishWorking)
            ? true : false;
    }

    async function clickHandler(e) {
        e.preventDefault();
        const task = e.currentTarget.getAttribute('data-task');
        console.log(task);

        let { workingDate, data } = getLocalEntry();

        if ((data && data.isDone) || isWorkDone()) {
            clearActiveBtn();
            setModal({ show: true, title: 'Task Info', body: 'You have completed your work for today!' });
        }
        else if (task !== 'start_working' && (!data || (data && data[workingDate] && !data[workingDate].startWorking))) {
            clearActiveBtn();
            setModal({ show: true, title: 'Task Info', body: 'Please start working first?' });
        }
        else if (task === 'finish_break' && !isBreakRunning()) {
            setModal({ show: true, title: 'Task Info', body: 'Please start break first?' });
        }
        else if (task === 'finish_working' && !isWorkRunning()) {
            clearActiveBtn();
            setModal({ show: true, title: 'Task Info', body: 'Please start working first?' });
        }
        else if ((task === 'finish_working' || task === 'start_break') && isBreakRunning()) {
            setModal({ show: true, title: 'Task Info', body: 'Please finish break first?' });
        }
        else if (task === 'start_working') {
            if (!data) {
                data = { [workingDate]: { workingDate } };
                data[workingDate].startWorking = moment().format('HH:mm'); // 16:45
                clearActiveBtn();
                setActiveBtn(task);
            } else if (data[workingDate] && data[workingDate].startWorking) { // users click start_working again
                setModal({ show: true, title: 'Task Info', body: 'You have already started working!' });
            }
        }
        else if (task === 'finish_working') {
            if (data[workingDate] && !data[workingDate].finishWorking) {
                data[workingDate].finishWorking = moment().format('HH:mm');
                clearActiveBtn();
                setActiveBtn(task);
                setShowMemo({ display: "block" }); // hand over to memo form
            }
            else if (data[workingDate] && data[workingDate].finishWorking) {
                clearActiveBtn();
                setAlert({ show: true, message: 'You have already finished working!', type: 'info' });
            }
        } // user may take break multiple times a day
        else if (task === 'start_break') {
            if (data[workingDate] && !data[workingDate].breaks) { // first break
                data[workingDate].breaks = [];
            }
            data[workingDate].breaks.push({ startBreak: moment().format('HH:mm') });
            clearActiveBtn();
            setActiveBtn(task);
        }
        else if (task === 'finish_break') {
            if (isBreakRunning()) {
                data[workingDate].breaks[data[workingDate].breaks.length - 1].finishBreak = moment().format('HH:mm');
                clearActiveBtn();
                setActiveBtn('start_working');
            } else {
                setModal({ show: true, title: 'Task Info', body: "You haven't started break!" });
            }
        }

        console.log('working data:', data);
        if (data) localStorage.setItem(workingDate, JSON.stringify(data));

    }

    async function submitHandler(e) {
        e.preventDefault();
        setSubmitBtn('Submitting...');
        let { workingDate, data } = getLocalEntry();
        data[workingDate].memo = textareaRef.current.value || '';
        console.log(data, data[workingDate]);
        // store in database
        let preInfo = await firebaseClient.getEntry(user.uid, data[workingDate].workingDate);
        if (preInfo) {
            window.localStorage.setItem(workingDate, JSON.stringify({ isDone: true }));
            textareaRef.current.value = '';
            setShowMemo({ display: "none" });
            clearActiveBtn();
            setModal({ show: true, title: 'Add unsuccessful!', body: 'You have record with this date!' });
        } else {
            let info = await firebaseClient.addEntry(user.uid, data[workingDate]);
            if (info && info.code) setModal({ show: true, title: info.code, body: info.message });
            else {
                window.localStorage.setItem(workingDate, JSON.stringify({ isDone: true }));
                textareaRef.current.value = '';
                setShowMemo({ display: "none" });
                clearActiveBtn();
                setModal({ show: true, title: 'Added successfully!', body: 'Your entry has been added.' });
            }
        }

    }

    return (
        <>
            <div className="row">
                <div className="taskbar">
                    <div className="card">
                        <div className="card-body">
                            <p className="card-title">Clock is ticking...</p>
                            <h3 style={{ marginBottom: 0 }}><Clock /></h3>
                        </div>
                    </div>
                </div>
                <div className="taskbar">
                    <div className="card text-center">
                        <a href="#" data-task="start_working" onClick={clickHandler}>
                            <div className="card-body">
                                <h5 className="card-title"><img src="/images/start_working.png" alt="start working" className="img-fluid img-max" /></h5>
                                <p className="card-text">start working</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div className="taskbar">
                    <div className="card text-center">
                        <a href="#" data-task="finish_working" onClick={clickHandler}>
                            <div className="card-body">
                                <h5 className="card-title"><img src="/images/finish_working.png" alt="finish working" className="img-fluid img-max" /></h5>
                                <p className="card-text">finish working</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div className="taskbar">
                    <div className="card text-center">
                        <a href="#" data-task="start_break" onClick={clickHandler}>
                            <div className="card-body">
                                <h5 className="card-title"><img src="/images/start_break.png" alt="start break" className="img-fluid img-max" /></h5>
                                <p className="card-text">start break</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div className="taskbar">
                    <div className="card text-center">
                        <a href="#" data-task="finish_break" onClick={clickHandler}>
                            <div className="card-body">
                                <h5 className="card-title"><img src="/images/finish_break.png" alt="finish break" className="img-fluid img-max" /></h5>
                                <p className="card-text">finish break</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div className="row" style={showMemo}>
                <form onSubmit={submitHandler}>
                    <div className="form-group col-sm-7">
                        <br />
                        <label htmlFor="memo">Memo</label>
                        <textarea className="form-control" id="memo" rows="3" required ref={textareaRef} required></textarea> <br />
                        <button type="submit" className="btn btn-primary btn-md btn-block">{submitBtn}</button>
                    </div>
                </form>
            </div>
        </>

    )

}
