import { useState } from "react";
import { getAuth, signInWithEmailAndPassword, createUserWithEmailAndPassword, signOut } from "firebase/auth";
import firebaseClientInit from "../firebase/client";
import { useAuth } from "../hook/auth";
import { useModal } from "../hook/modal";


export default function Login(props) {
    firebaseClientInit();
    const { user } = useAuth();
    const { modal, setModal } = useModal();
    const [email, setEmail] = useState('');
    const [password, setPass] = useState('');
    const [loginBtnText, setLoginBtnText] = useState('Log in');
    const [logoutBtnText, setLogoutBtnText] = useState('Log out');

    function loginHandler(e) {
        e.preventDefault();
        setLoginBtnText('Logging in...');
        const auth = getAuth();
        signInWithEmailAndPassword(auth, email, password)
            .then((userCredential) => { // Signed in
                setLoginBtnText('Logged in');
                console.log(userCredential);
                // setModal({ show: true, title: 'Login successful!', body: `Welcome ${userCredential.user.email}` });
            })
            .catch((error) => {
                console.log(error);
                setLoginBtnText('Login failed!');
                setModal({ show: true, title: 'Login failed!', body: `[${error.code}] ${error.message}` });
            });
    }

    function logoutHandler(e) {
        e.preventDefault();
        setLogoutBtnText('Logging out...');
        const auth = getAuth();
        signOut(auth).then(() => {
            setLogoutBtnText('Logged out');
        })
        .catch((error) => {
            console.log(error);
            setLogoutBtnText('Logout failed!');
            setModal({ show: true, title: 'Logout failed!', body: `[${error.code}] ${error.message}` });
        });
    }

    return (
        <>
            {
                user && user.role && (user.role.admin || user.role.member) ? (
                    <div className="text-center">
                        <form className="form-signin" onSubmit={logoutHandler}>
                            <h5>Hello, {user.email} !</h5>
                            <button className="btn btn-lg btn-primary btn-block" type="submit">{logoutBtnText}</button>
                        </form>
                    </div>
                ) : (
                    <form className="form-signin" onSubmit={loginHandler}>
                        <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
                        <label htmlFor="inputEmail" className="sr-only">Email address</label>
                        <input type="email" className="form-control" placeholder="Email address" required autoFocus onChange={(e) => setEmail(e.target.value)} />
                        <label htmlFor="inputPassword" className="sr-only">Password</label>
                        <input type="password" className="form-control" placeholder="Password" required onChange={(e) => setPass(e.target.value)} />
                        <button className="btn btn-lg btn-primary btn-block" type="submit">{loginBtnText}</button>
                    </form >
                )
            }
        </>
    )

}
