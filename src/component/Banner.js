export default function Banner(props) {
    return (
        <header className="blog-header py-3 text-center">
            <h2>{props.pageName || 'Attendance Management'}</h2>
        </header>
    )
}
