import { useRouter } from "next/router"


export default function ContentHeader(props) {
    const router = useRouter();

    function clickHandler(e) {
        if (props.btn.href) {
            router.push(props.btn.href);
        }
    }

    return (
        <div className="row">
            <div className="col-md-8 order-md-1">
                <h4>{props.title}</h4>
            </div>

            {props.btn && (
                <div className="col-md-4 order-md-2 mb-4">
                    <button type="button" className="btn btn-primary btn-md btn-block float-right" onClick={clickHandler}>{props.btn.name}</button>
                </div>
            )}
        </div>
    )
}