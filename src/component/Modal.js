export default function Modal({ closeHandler, show, title, body }) {
    const showHide = show ? { display: "block" } : { display: "none" };

    return (
        <div className="modal" tabIndex="-1" role="dialog" style={showHide}>
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">{title}</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={closeHandler}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <p>{body}</p>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={closeHandler}>Close</button>
                    </div>
                </div>
            </div>
        </div>
    )

}
