import { useRouter } from "next/router";
import { useRef, useState } from "react";
import * as firebaseClient from "../firebase/client";
import { useAuth } from "../hook/auth";
import { useModal } from "../hook/modal";
import { useAlert } from "../hook/alert";


export default function AddUpdateEntry(props) {
    const { user } = useAuth();
    const router = useRouter();
    const { modal, setModal } = useModal();
    const { alert, setAlert } = useAlert();
    const [addBtn, setAddBtn] = useState(props.isUpdate ? 'Update Entry' : 'Add Entry');
    const workingDate = useRef();
    const startWorking = useRef();
    const finishWorking = useRef();
    const startBreak = useRef();
    const finishBreak = useRef();
    const memo = useRef();

    async function submitHandler(e) {
        e.preventDefault();
        setAddBtn(props.isUpdate ? 'Updating Entry...' : 'Adding Entry...');
        let data = {
            workingDate: workingDate.current.value,
            startWorking: startWorking.current.value,
            finishWorking: finishWorking.current.value,
            breaks: [
                {
                    startBreak: startBreak.current.value,
                    finishBreak: finishBreak.current.value
                }
            ],
            memo: memo.current.value.trim()
        };
        console.log('newEntryData', data);

        let preInfo = await firebaseClient.getEntry(user.uid, data.workingDate);
        if (preInfo && !props.isUpdate) {
            setAddBtn('Add Failed!');
            setModal({ show: true, title: 'Add unsuccessful!', body: 'You have record with this date!' });
        } else {
            // for update operation only
            // user change working date, so delete previous one before add new date
            if (props.isUpdate && props.entry.workingDate !== data.workingDate) {
                await firebaseClient.deleteEntry(user.uid, props.entry.workingDate);
            }
            let info = await firebaseClient.addEntry(user.uid, data);
            if (info && info.code) {
                setAddBtn(props.isUpdate ? 'Update Failed!' : 'Add Failed!');
                setModal({ show: true, title: info.code, body: info.message });
            } else {
                setAddBtn(props.isUpdate ? 'Updated!' : 'Added!');
                setAlert({ show: true,
                    message: `Attendance dated ${data.workingDate} has been ${props.isUpdate ? 'updated' : 'added'} for <${user.email}>`,
                    type: 'success'
                });
                router.push('/member');
            }
        }

    }

    return (
        <div>
            <h4 className="mb-3">{props.isUpdate ? 'Update' : 'Add'} Entry</h4>
            <form onSubmit={submitHandler}>
                <div className="form-group row">
                    <label htmlFor="working-date" className="col-sm-2 col-form-label">Working date</label>
                    <div className="col-sm-5">
                        <input type="date" className="form-control" id="working-date" ref={workingDate} defaultValue={props.entry.workingDate} required />
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="start-working" className="col-sm-2 col-form-label">Start working</label>
                    <div className="col-sm-5">
                        <input type="time" className="form-control" id="start-working" ref={startWorking} defaultValue={props.entry.startWorking} required />
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="finish-working" className="col-sm-2 col-form-label">Finish working</label>
                    <div className="col-sm-5">
                        <input type="time" className="form-control" id="finish-working" ref={finishWorking} defaultValue={props.entry.finishWorking} required />
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="start-break" className="col-sm-2 col-form-label">Start break</label>
                    <div className="col-sm-5">
                        <input type="time" className="form-control" id="start-break" ref={startBreak} defaultValue={props.entry.breaks && props.entry.breaks[0].startBreak} required />
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="finish-break" className="col-sm-2 col-form-label">Finish break</label>
                    <div className="col-sm-5">
                        <input type="time" className="form-control" id="finish-break" ref={finishBreak} defaultValue={props.entry.breaks && props.entry.breaks[0].finishBreak} required />
                    </div>
                </div>
                <div className="form-group col-sm-7">
                    <label htmlFor="memo">Memo</label>
                    <textarea className="form-control" id="memo" rows="3" required ref={memo} defaultValue={props.entry.memo} ></textarea>
                </div>
                <br />
                <div className="form-group row">
                    <div className="col-sm-7">
                        <button type="submit" className="btn btn-primary btn-lg btn-block">{addBtn}</button>
                    </div>
                </div>
            </form>
        </div>
    )

}