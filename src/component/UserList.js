import Link from "next/link"
import Loading from "./Loading";
import { useState, useEffect } from "react"
import { useModal } from "../hook/modal";


export default function List() {
    const { modal, setModal } = useModal();
    const [users, setUsers] = useState([]);

    useEffect(() => {
        fetch('/api/admin/all', {
            method: 'POST'
        }).then(async (res) => {
            let _res = await res.json();
            console.log(_res);
            setUsers(_res.message.users);
        }).catch((err) => {
            console.log(err);
        });
    }, [])

    function handleDelete(e) {
        e.preventDefault();
        const uid = e.target.getAttribute('data-uid');

        fetch('/api/admin/delete/' + uid, {
            method: 'POST'
        }).then(async (res) => {
            let _res = await res.json();
            console.log(_res);
            if (_res.status) {
                let updatedUsers = users.filter((user) => user.uid !== uid);
                setUsers(updatedUsers);
            }
            let title = (_res.status) ? "Deleted successfully!" : "Deletion failed!";
            setModal({ show: true, title: title, body: `[${_res.code}] ${_res.message}` });
        }).catch((err) => {
            console.log(err);
            setModal({ show: true, title: "Deletion failed!", body: `[${err.code}] ${err.message}` });
        });
    }

    if (users.length) {
        return (
            <table className="table table-sm table-hover">
                <thead>
                    <tr className="d-flex">
                        <th className="col-1"></th>
                        <th className="col-5">Name</th>
                        <th className="col-5 d-none d-sm-table-cell">Email</th>
                        <th className="col-1">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {users.map((user) => {
                        return (
                            <tr className="d-flex" key={user.uid}>
                                <td className="col-1">
                                    <Link href={`/admin/record/${user.uid}`}>
                                        <img src={user.photoURL || '/images/default-profile-img.png'} alt={user.displayName} className="rounded-circle img-profile" />
                                    </Link>
                                </td>
                                <td className="col-5">
                                    <Link href={`/admin/record/${user.uid}`}>{user.displayName || '—'}</Link>
                                </td>
                                <td className="col-5 d-none d-sm-table-cell">{user.email || '—'}</td>
                                <td className="col-1">
                                    <div className="dropdown">
                                        <i className="btn btn-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            &nbsp;&nbsp;<Link className="dropdown-item" href={`/admin/record/${user.uid}`}>View record</Link><br />
                                            &nbsp;&nbsp;<Link className="dropdown-item" href={`/admin/update/${user.uid}`}>Update</Link><br />
                                            &nbsp;&nbsp;<Link className="dropdown-item" href="#">
                                                <a href="#" data-uid={user.uid} onClick={handleDelete}>Delete</a>
                                            </Link><br />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        )
    } else return <Loading />

}