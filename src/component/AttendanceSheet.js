import moment from "moment";
import Link from "next/link";
import { useEffect, useRef, useState } from "react";
import { useModal } from "../hook/modal";
import * as firebaseClient from "../firebase/client";
import Loading from "./Loading";


export default function ActivityList(props) {
    const [sheet, setSheet] = useState([]);
    const { modal, setModal } = useModal();
    const fromDateRef = useRef();
    const toDateRef = useRef();

    useEffect(() => {
        console.log('...................');
        (async () => {
            let activities = await firebaseClient.getAttendance(props.uid, moment().format('YYYY-MM-DD'));
            console.log(activities);
            let temp = [];
            for (const key in activities) {
                temp.push(activities[key]);
            }
            console.log('temp', temp);
            setSheet(temp.length > 0 ? temp : 'No record found!');
        })()
    }, [])

    async function submitHandler(e) {
        e.preventDefault();
        let from = fromDateRef.current.value;
        let to = toDateRef.current.value;

        if (from && to && moment(to, 'YYYY-MM-DD').diff(moment(from, 'YYYY-MM-DD')) > 2592000000) {
            setModal({ show: true, title: 'Query Info', body: 'Range is huge to display, provide small..' });
        } else if (from && to && moment(to, 'YYYY-MM-DD').diff(moment(from, 'YYYY-MM-DD')) >= 0) {
            let temp = [];
            let activities = await firebaseClient.getAttendance(props.uid, from);
            for (const key in activities) {
                temp.push(activities[key]);
            }
            if (from.slice(0, -3) !== to.slice(0, -3)) { // two different months
                let activities = await firebaseClient.getAttendance(props.uid, to);
                for (const key in activities) {
                    temp.push(activities[key]);
                }
            }
            let filterItems = temp.filter((item) => { // 4  15  24
                if (moment(item.workingDate, 'YYYY-MM-DD').diff(moment(from, 'YYYY-MM-DD')) >= 0 &&
                    moment(item.workingDate, 'YYYY-MM-DD').diff(moment(to, 'YYYY-MM-DD')) <= 0
                ) return true;
                else return false;
            })
            setSheet(filterItems);
        } else {
            setModal({ show: true, title: 'Query Info', body: 'Please provide a valid range!' });
        }

    }

    function sortByDate(arr) {
        return arr.sort(function (a, b) {
            let aDate = moment(a.workingDate, 'YYYY-MM-DD').valueOf();
            let bDate = moment(b.workingDate, 'YYYY-MM-DD').valueOf();
            return aDate - bDate;
        });
    }

    function isHoliDay(workingDate) {
        return ['Sat', 'Sun'].includes(moment(workingDate, 'YYYY-MM-DD').format('ddd'));
    }

    function colorSatSun(workingDate) {
        let temp = moment(workingDate, 'YYYY-MM-DD').format('ddd');
        if (temp === 'Sat') return <td style={{ color: 'blue' }}>{temp}</td>
        else if (temp === 'Sun') return <td style={{ color: 'red' }}>{temp}</td>
        else return <td>{temp}</td>
    }

    function totalWorkHrs(activity) {
        let totalBreaks = subtractTime(activity.breaks[0].startBreak, activity.breaks[0].finishBreak, true);
        let totalWorking = subtractTime(activity.startWorking, activity.finishWorking, true)
        return minToHrMin(totalWorking - totalBreaks);
    }

    function minToHrMin(mins) {
        let hours = Math.floor(mins / 60);
        let minutes = mins % 60;
        let str = hours ? (hours > 1 ? hours + ' hrs ' : hours + ' hr ') : '';
        str += minutes ? (minutes > 1 ? minutes + ' mins' : minutes + ' min') : '';
        return !str ? '~1 min' : str;
    }

    function subtractTime(time1, time2, inMin) {
        time1 = time1.split(':');
        time2 = time2.split(':');
        // convert 16:35 -> 16*60+35 mins
        time1 = (parseInt(time1[0]) * 60) + parseInt(time1[1]);
        time2 = (parseInt(time2[0]) * 60) + parseInt(time2[1]);
        let diff = time2 - time1;
        if (inMin) return diff;
        else return minToHrMin(diff);
    }

    function subtractTime2(date1, time1, date2, time2) {
        let start = moment(`${date1} ${time1}`, 'YYYY-MM-DD HH:mm').toDate().getTime();
        let end = moment(`${date2} ${time2}`, 'YYYY-MM-DD HH:mm').toDate().getTime();
        let timespan = end - start;
        let duration = moment(timespan);
        return duration.format('HH:mm');
    }

    return (
        <>{(Array.isArray(sheet) && sheet.length > 0) ? 
            (<>
                <div>
                    <br />
                    <form className="form-inline" onSubmit={submitHandler}>
                        <div className="input-group mb-2 mr-sm-2">
                            <label className="form-check-label" htmlFor="from-date">
                                From:&nbsp;
                            </label>
                            <input type="date" className="form-control" id="from-date" ref={fromDateRef} defaultValue={moment().startOf('month').format('YYYY-MM-DD')} required />
                        </div>

                        <div className="form-check mb-2 mr-sm-2">
                            <label className="form-check-label" htmlFor="to-date">
                                to:&nbsp;
                            </label>
                            <input type="date" className="form-control" id="to-date" ref={toDateRef} defaultValue={moment().endOf('month').format('YYYY-MM-DD')} required />
                        </div>

                        <button type="submit" className="btn btn-primary mb-2">Submit</button>
                    </form>
                    <br />
                </div>

                <div className="table-responsive">
                    <table className="table table-bordered table-hover table-sm">
                        <thead className="table-active" style={{ fontWeight: 'bold' }}>
                            <tr>
                                <td scope="col" rowSpan="2">Edit</td>
                                <td scope="col" rowSpan="2">Date</td>
                                <td scope="col" rowSpan="2">Day</td>
                                <td scope="col" rowSpan="2">Category</td>
                                <td scope="col" rowSpan="2">Start Working</td>
                                <td scope="col" rowSpan="2">Finish Working</td>
                                <td scope="col" colSpan="2">Breaks</td>
                                <td scope="col" rowSpan="2">Total Work Hours</td>
                                <td scope="col" rowSpan="2">Total Break Hours</td>
                                <td scope="col" rowSpan="2">Memo</td>
                            </tr>
                            <tr>
                                <td scope="col">Start</td>
                                <td scope="col">Finish</td>
                            </tr>
                        </thead>
                        <tbody>
                            {sheet.map((activity) => {
                                return (
                                    <tr key={activity.workingDate} className={!isHoliDay(activity.workingDate) ? 'table-danger' : ''}>
                                        {/* edit */}
                                        <td>
                                            <b><Link href={'/member/update/' + activity.workingDate}>Edit</Link></b>
                                        </td>
                                        {/* Date */}
                                        <td>{moment(activity.workingDate, 'YYYY-MM-DD').format('DD')}</td>
                                        {/* Day */}
                                        {colorSatSun(activity.workingDate)}
                                        {/* Category */}
                                        <td>{isHoliDay(activity.workingDate) ? 'Holiday' : 'Work Day'}</td>
                                        {/* Start Working */}
                                        <td>{moment(activity.startWorking, 'HH:mm').format('hh:mm a')}</td>
                                        {/* Finish Working */}
                                        <td>{moment(activity.finishWorking, 'HH:mm').format('hh:mm a')}</td>
                                        {/* Break Start */}
                                        <td>{moment(activity.breaks[0].startBreak, 'HH:mm').format('hh:mm a')}</td>
                                        {/* Break Finish */}
                                        <td>{moment(activity.breaks[0].finishBreak, 'HH:mm').format('hh:mm a')}</td>
                                        {/* Total Work Hours */}
                                        <td>{totalWorkHrs(activity)}</td>
                                        {/* Total Break Hours */}
                                        <td>{subtractTime(activity.breaks[0].startBreak, activity.breaks[0].finishBreak)}</td>
                                        {/* Memo */}
                                        <td>{activity.memo}</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </>
            ) : sheet === 'No record found!' ? <h5 className="text center">{sheet}</h5> : <Loading />
        }</>
        
    )


}
