import { useState, useRef } from "react";


export default function UserInfoForm(props) {
    const fullnameRef = useRef();
    const emailRef = useRef();
    const passRef = useRef();
    const roleRef = useRef();
    const [selectedFile, setSelectedFile] = useState('');

    function submitHandler(e) {
        e.preventDefault();
        props.setFormInfo({
            displayName: fullnameRef.current.value,
            email: emailRef.current.value,
            password: passRef.current.value,
            role: roleRef.current.value,
            selectedFile: selectedFile
        })
    }

    return (
        <div className="col-md-8 order-md-1">
            <h4 className="mb-3">{props.user.uid ? 'Update Employee' : 'Create Employee'}</h4>
            <form className="needs-validation" encType="multipart/form-data" onSubmit={submitHandler}>
                <div className="mb-3">
                    <label htmlFor="fullname">Fullname</label>
                    <input type="text" className="form-control" ref={fullnameRef} defaultValue={props.user.displayName} placeholder="Tareq Ahamed" required />
                </div>

                <div className="mb-3">
                    <label htmlFor="email">Email</label>
                    <input type="email" className="form-control" ref={emailRef} defaultValue={props.user.email} placeholder="you@example.com" required />
                </div>

                <div className="mb-3">
                    <label htmlFor="password">Password</label>
                    <input type="password" className="form-control" ref={passRef} placeholder="******" required />
                </div>

                <div className="mb-3">
                    <label htmlFor="role">Role</label>
                    <select className="custom-select my-1 mr-sm-2" ref={roleRef} required>
                        {props.user.role && props.user.role.admin ?
                            (
                                <>
                                    <option>admin</option>
                                    <option>member</option>
                                    <option></option>
                                </>
                            ) : props.user.role && props.user.role.member ?
                                (
                                    <>
                                        <option>member</option>
                                        <option>admin</option>
                                        <option></option>
                                    </>
                                ) : (
                                    <>
                                        <option></option>
                                        <option>admin</option>
                                        <option>member</option>
                                    </>
                                )
                        }
                    </select>
                </div>

                <p>Profile Picture</p>
                {(props.user.uid) ? (
                    <div>
                        <input type="file" onChange={(e) => setSelectedFile(e.target.files[0])} accept="image/*" />
                        <span><img src={props.user.photoURL || '/images/default-profile-img.png'} alt={props.user.displayName} className="img-thumbnail" /></span>
                    </div>
                ) : (
                    <input type="file" onChange={(e) => setSelectedFile(e.target.files[0])} accept="image/*" required />
                )}

                <br /><br />
                <button className="btn btn-primary btn-lg btn-block" type="submit">{props.btnText}</button>
            </form>
        </div>
    )

}