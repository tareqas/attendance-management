# Attendance Management

made with [NextJS](https://nextjs.org/), [Bootstrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/) and ❤️

live @ https://attendance-management-tk.herokuapp.com

### Sample Admin Account
email: mail@example.com <br>
password: 123456

Some of these features are -
## Member Panel
- Record Attendance
- Manually Input Attendance
- Update Attendance
- View itself Attendance Record

## Admin Panel
- All features of Member Panel
- Create New Employee
- Update Employee
- Delete Employee
- View Employee Attendance Record

and so on.
