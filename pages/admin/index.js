import UserList from "../../src/component/UserList";
import { WithProtected } from "../../src/hook/route";
import ContentHeader from "../../src/component/ContentHeader";


function Admin() {
    return (
        <>
            <ContentHeader title={'Employee Lists'} btn={{ name: 'Create Employee', href: '/admin/new' }}/>
            <UserList />
        </>
    )

}

export default WithProtected(Admin);
