import { useRouter } from "next/router"
import { WithProtected } from "../../../src/hook/route";
import AttendanceSheet from "../../../src/component/AttendanceSheet";
import ContentHeader from "../../../src/component/ContentHeader";


function Record() {
    const router = useRouter();
    const { uid } = router.query;

    return (
        <>
            <ContentHeader title={'User['+uid+']: activities'}/>
            <AttendanceSheet uid={uid} />
        </>
    )
}

export default WithProtected(Record);
