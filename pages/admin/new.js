import CreateUpdate from "../../src/component/CreateUpdate";
import { WithProtected } from "../../src/hook/route";


function NewEmployee() {
    return (
        <CreateUpdate />
    )
}

export default WithProtected(NewEmployee);
