import { useRouter } from "next/router"
import { useState, useEffect } from "react";
import { useModal } from "../../../src/hook/modal";
import CreateUpdate from "../../../src/component/CreateUpdate";
import { WithProtected } from "../../../src/hook/route";


function Update() {
    const router = useRouter();
    const { uid } = router.query;
    const [user, setUser] = useState();
    const { modal, setModal } = useModal();

    useEffect(() => {
        if (uid) {
            fetch('/api/admin/user/' + uid, {
                method: 'POST'
            }).then(async (res) => {
                let _res = await res.json();
                console.log('-------------------', _res);
                if (_res.status) {
                    let { uid, displayName, email, photoURL } = _res.message;
                    let role = _res.message.customClaims;
                    setUser({ uid, displayName, email, photoURL, role });
                } else {
                    setModal({ show: true, title: 'fetch_err', body: `[${_res.code}] ${_res.message}` });
                }
            }).catch((err) => {
                console.log(err);
                setModal({ show: true, title: 'fetch_err', body: `[${err.code}] ${err.message}` });
            });
        }
    }, [uid])

    return <CreateUpdate isUpdate={true} user={user} />

}

export default WithProtected(Update);
