function Home(props) {
    return (
        <div>
            <h5 className="text-center">Hello there, let&#x27;s check out some of these features:</h5>
            <br/>
            <div className="row">
                <div className="col-sm">
                    <ul className="list-group">
                        <li className="list-group-item active">Member Panel</li>
                        <li className="list-group-item">Record Attendance</li>
                        <li className="list-group-item">Manually Input Attendance</li>
                        <li className="list-group-item">Update Attendance</li>
                        <li className="list-group-item">View himself/herself Attendance Record</li>
                    </ul>
                </div>

                <div className="col-sm">
                    <ul className="list-group">
                        <li className="list-group-item active">Admin Panel</li>
                        <li className="list-group-item">All features of Member Panel</li>
                        <li className="list-group-item">Create New Employee</li>
                        <li className="list-group-item">Update Employee</li>
                        <li className="list-group-item">Delete Employee</li>
                        <li className="list-group-item">View Employee Attendance Record</li>
                    </ul>
                </div>
            </div>

        </div>
    )
}

export default Home;
