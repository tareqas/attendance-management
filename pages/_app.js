import { AuthProvider } from "../src/hook/auth";
import AppLayout from "../src/layout/AppLayout";

function MyApp({ Component, pageProps }) {
  return (
    <AuthProvider>
      <AppLayout>
        <Component {...pageProps} />
      </AppLayout>
    </AuthProvider>
  )
}

export default MyApp
