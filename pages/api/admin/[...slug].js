import * as firebaseAdmin from "../../../src/firebase/admin";
import authMiddleware from "../../../src/server/authMiddleware";


export default async function handler(req, res) {
    await authMiddleware(req, res).then((_res) => _res);
    let { uid, email } = req.locals;

    let [opcode, id] = req.query.slug;
    let body = req.body ? JSON.parse(req.body) : {};

    if (opcode === 'new' && !id && req.method === 'POST') {
        let user = await firebaseAdmin.getUserByEmail(body.email); // first check 'does user exist'
        if (user.code) { // no user found
            if (body && body.displayName && body.email && body.password && body.role && body.photoURL) {
                let url = await firebaseAdmin.getPublicUrl(body.photoURL); // create public url from private for profile pic
                if (!url.code) {
                    body.photoURL = url; // replace private url with public
                    let _user = await firebaseAdmin.createUser(body);
                    if (_user.uid) { // after user creation, set role of user, admin/member
                        let info = await firebaseAdmin.setCustomUserClaims(_user.uid, { [body.role]: true });
                        if (info.code) { // if role setting is failed, delete newly created account
                            await firebaseAdmin.deleteUser(_user.uid);
                            res.json({ status: false, code: 'not_created', message: 'try again later!' });
                        }
                        else res.json({ status: true, code: 'created', message: `${_user.displayName} <${_user.email}>`, extra: { uid: _user.uid } });
                    }
                    else res.json({ status: false, code: _user.code, message: _user.message });
                }
                else res.json({ status: false, code: url.code, message: url.message });
            }
            else res.json({ status: false, code: 'not_created', message: 'please valid information!' });
        }
        else res.json({ status: false, code: 'not_created', message: `${user.displayName} <${user.email}> already exits!` });

    }
    else if (opcode === 'update' && id && req.method === 'POST' &&
        body && body.displayName && body.email && body.password && body.role
    ) {
        // if user sends new photo, get public url. Otherwise delete 'photoURL' to keep previous photo
        let url = (body.photoURL) ? await firebaseAdmin.getPublicUrl(body.photoURL) : delete body.photoURL;
        // if new photo updated successfully, add it to body.photoURL to update
        (!url.code && body.photoURL) ? body.photoURL = url : delete body.photoURL;
        let _user = await firebaseAdmin.updateUser(id, body);
        if (_user.uid) {
            let info = await firebaseAdmin.setCustomUserClaims(_user.uid, { [body.role]: true });
            if (info.code) {
                res.json({ status: false, code: info.code, message: 'try again later!' });
            }
            else res.json({ status: true, code: 'updated', message: `${_user.displayName} <${_user.email}>`, extra: { uid: _user.uid } });
        }
        else res.json({ status: false, code: _user.code, message: _user.message });

    }
    else if (opcode === 'delete' && id && req.method === 'POST') {
        let user = await firebaseAdmin.deleteUser(id);
        if (user) res.json({ status: true, code: 'deleted', message: `${id} deleted successfully!` });

    }
    else if (opcode === 'user' && req.method === 'POST') { // fetch user's info
        let info = await firebaseAdmin.getUser(id);
        if (info.code) res.json({ status: false, code: info.code, message: info.message });
        else res.json({ status: true, code: 200, message: info });

    }
    else if (opcode === 'all' && req.method === 'POST') { // fetch all users' info
        let info = await firebaseAdmin.listAllUsers(id);
        if (info.code) res.json({ status: false, code: info.code, message: info.message });
        else res.json({ status: true, code: 200, message: info });

    }
    else res.json({ status: false, code: 'no_opcode', message: 'provide a valid operation!' });

}

