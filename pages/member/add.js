import AddUpdateEntry from "../../src/component/AddUpdateEntry";
import { WithProtected } from "../../src/hook/route";


function NewEntry() {
    return (
        <AddUpdateEntry isUpdate={false} entry={{}}/>
    )
}

export default WithProtected(NewEntry);
