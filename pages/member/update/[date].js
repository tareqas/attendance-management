import { useRouter } from "next/router"
import { useState, useEffect } from "react";
import * as firebaseClient from "../../../src/firebase/client";
import AddUpdateEntry from "../../../src/component/AddUpdateEntry";
import { useAuth } from "../../../src/hook/auth";
import { WithProtected } from "../../../src/hook/route";


function Update() {
    const router = useRouter();
    const { date } = router.query;
    const { user } = useAuth();
    const [entry, setEntry] = useState();

    useEffect(() => {
        if (user && date) {
            (async () => {
                let entry = await firebaseClient.getEntry(user.uid, date);
                if (entry) setEntry(entry);
            })()
        }
    }, [user, date])

    return (
        <>
            {entry && <AddUpdateEntry isUpdate={true} entry={entry} />}
        </>
    )

}

export default WithProtected(Update);
