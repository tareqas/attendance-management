import AttendanceSheet from "../../src/component/AttendanceSheet";
import { WithProtected } from "../../src/hook/route";
import { useAuth } from "../../src/hook/auth";
import ContentHeader from "../../src/component/ContentHeader";
import Loading from "../../src/component/Loading";


function Member() {
    const { user } = useAuth();

    return (
        <>
            {user && user.role && (user.role.admin || user.role.member) ? (<>
                <ContentHeader title={'Your activities'} btn={{ name: 'Add Attendance', href: '/member/add' }} />
                <AttendanceSheet uid={user.uid} />
            </>) : <Loading />
            }
        </>
    )

}

export default WithProtected(Member);
